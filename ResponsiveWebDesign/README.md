Responsive Web Design
=====================

## ¿Qué es Responsive Web Design?

El **Responsive Web Design** o **Adaptative Web Design**, son técnicas que se usan en la actualidad para tener una misma web adaptada a las diferentes plataformas que nos brinda la tecnología: `desktop`, `tablets` y/o `mobile`.
Consiste en una serie de hojas de estilo en CSS3, que usando los _media queries_ convierten una web ordinaria en una web multiplataforma capaz de adaptarse a todos los tamaños que existen, ofreciendo una mejor experiencia usuario.

### ¿Por qué tu web debería ser responsive?
Las ventajas del **Responsive Web Design** son muchas, pero las que considero más destacadas son:
 + **Posicionamiento SEO**. Google prioriza en los resultados de búsquedas aquellas webs que cuentan con un **Responsive Web Design**. De esta forma, no sólo se obtienen mejores puestos en el ranking, sino que además se cubre un mayor tráfico al ser un sitio web visible desde cualquier dispositivo.
 + **Experiencia del usuario**. Un sitio web que sea responsive favorece la usabilidad, Otro de los conceptos más importantes del diseño web y que se traduce en una mejor experiencia de navegación; que propiciará un aumento en el ratio de conversión de la página.
 + **Evita la duplicidad del contenido**. Un **Responsive Web Design** hace que no sea necesario repetir el mismo contenido para cada versión de una web. Recurriendo a esta técnica de diseño, es posible proporcionar el mismo contenido a todos los usuarios y se contribuye a la mejora de la experiencia de navegación.
 + **Reducción de costos, tiempos de desarrollo y mantenimiento**. Gracias a un **Responsive Web Design** se evita tener que desarrollar diferentes versiones de la web para cada dispositivo. De esta forma, además, las tareas de mantenimiento se simplifican y agilizan, ya que sólo será necesario realizar cambios en una URL; en lugar de tener que modificar cada una de las versiones existentes.

#### Un ejemplo simple de por qué utilizarlo
Su contenido puede estar separado en diferentes columnas en `desktop`.
Perooooo... Si separa su contenido en varias columnas en `mobile`, será difícil para los usuarios leer e interactuar con él.

## ¿Responsive Web Design o Adaptative Web Design?

El **Responsive Web Design** y el **Adaptative Web Design** son conceptos muy parecidos, pero efectivamente no son lo mismo.
Ambos son métodos de programación flexible que adaptan la web en función del dispositivo para asegurar su visualización.
Entonces, ¿Cuál es la diferencia?

La principal diferencia es que el **Responsive Web Design** se adapta literalmente.
El **Adaptative Web Design**, se hace un diseño para cada dispositivo (varias resoluciones preestablecidas como `desktop`, `tablets` y/o `mobile` (o cualquier otra resolución/dispositivo que se desee soportar)).

#### Responsive Web Design.
 1. Reestructura todos los elementos de la web para optimizar todo el espacio y asegurar una buena experiencia de usuario, un excelente aspecto visual y la funcionalidad.
 2. Un sólo diseño que se adapta según la pantalla.
 3. Utiliza tamaños proporcionales, en lugar de valores fijos en píxeles. Establece medidas en porcentajes.
 4. Emplea media queries y diferentes hojas de estilo (CSS) para cada medida de pantalla.
 5. Es muy flexible.
 6. Puede requerir mayor tiempo de carga.

#### Diseño web adaptativo.
 1. Utiliza tamaños de pantalla fijos y preestablecidos para cada uno de los dispositivos.
 2. Diferentes diseños de forma independiente en los que podemos variar lo que mostramos, aumentando la calidad de la experiencia de navegación del usuario.
 3. Utiliza valores fijos en píxeles.
 4. No necesita tanto código. Sencillez.
 5. No es tan flexible. No se ajusta exactamente a cualquier resolución.
 6. Menor tiempo de carga. El dispositivo recibe sólo lo necesario para su visualización.

## ¿Qué hay que tener en cuenta para diseñar una buena web responsive?
 + **Tipografías**. Evidentemente, el tamaño de letra tiene que ser diferente en función de la pantalla, de manera que podamos leer los textos sin necesidad de hacer zoom. Esto implica, por ejemplo, que no debemos incluir columnas con un ancho predeterminado en un sitio responsive. La familia tipográfica que escojamos también es muy importante a la hora de determinar la legibilidad.
 + **Multimedia (imágenes y vídeos)**. Los elementos visuales de la página deben seguir una proporción lógica en función del dispositivo donde se muestran, de manera que podamos verlos con comodidad.
 + **Orientación (horizontal o vertical)**. En particular, es necesario tener en cuenta que los usuarios de `mobile` suelen preferir el vertical, pero pueden alternar entre ambos para visualizar un contenido determinado.
 + **Usabilidad**. Los usuarios de `mobile` y tablets usan pantallas táctiles para interactuar con los contenidos, mientras que en los ordenadores esta interacción tiene lugar a través del ratón. Esto implica que los menús, los botones y demás elementos deben repensarse para ofrecer una buena experiencia de usuario en ambos casos.
 + **Tiempos de carga**. Intentar cargar una web de escritorio desde un teléfono móvil puede ser una experiencia extremadamente frustrante para el usuario y hacer que abandone fácilmente. Por eso, es necesario optimizar al máximo los tiempos de carga en todos los dispositivos.
 + **Efectos**. Por ejemplo, el hover funciona en ordenadores de escritorio, pero no en `mobile`, así que si colocas en él el "leer más" de un artículo o noticia los usuarios `mobile` no podrán acceder a él.

## Frameworks
Con un framework CSS podemos maquetar de una manera mucho más rápida un sitio web, gracias a los componentes, utilidades y documentación que nos ofrecen. Pero además utilizarlo nos ayudará a tener un código más limpio y más estructurado.
Estos son algunos de los más conocidos y mantenidos por la comunidad.
 + https://getbootstrap.com
 + https://tailwindcss.com
 + https://get.foundation
 + https://bulma.io

Tambien tenemos que tener en cuentas los pros y contras de usar un framework.
 + https://openwebinars.net/blog/ventajas-y-desventajas-de-usar-framework-css
 + https://desarrolloweb.com/articulos/framework-css-ventajas-inconvenientes.html

## Fuentes
#### Responsive Web Design.
 + https://kinsta.com/blog/responsive-web-design
 + https://www.cyberclick.es/que-es/diseno-web-responsive
 + https://www.iebschool.com/blog/que-es-responsive-web-design-analitica-usabilidad
 + https://developer.mozilla.org/es/docs/Web/CSS/Media_Queries/Using_media_queries
 + https://www.40defiebre.com/que-es/diseno-responsive

#### Mobile First
 + https://www.initcoms.com/que-es-mobile-first-posicionamiento

#### Flex
 + https://tobiasahlin.com/blog/common-flexbox-patterns
 + https://developer.mozilla.org/es/docs/Web/CSS/flex
