EJERCICIO
=========

## Blog

Crear un blog simple.

### Ejemplo
https://culturayciencia.diariocronicas.com

### ¿Qué se espera obtener?
Un sitio web que se visualice correctamente en 3 tipos de dispositivos `desktop`, `tablet` y `mobile`.
 + `desktop` 3 post por fila.
 + `tablet` 2 post por fila.
 + `mobile` 1 post por fila.

No se pueden utilizar frameworks.

### Puntos extras
Además de la versión vainilla CSS. Se sumarán puntos extras por agregar una versión utilizando un framework (Incorporarlo con CDN).

